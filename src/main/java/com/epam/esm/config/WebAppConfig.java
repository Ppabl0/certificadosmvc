package com.epam.esm.config;

import com.epam.esm.controller.GiftCertController;
import com.epam.esm.repository.*;
import com.epam.esm.service.GiftCertService;
import com.epam.esm.service.TagService;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.sql.DataSource;

@Configuration
@EnableWebMvc
@PropertySource("classpath:postgres.properties")
@ComponentScan(basePackageClasses = {PostgresGiftCertsRepo.class, GiftCertController.class, GiftCertService.class,
                                     TagService.class, GiftCertRowMapper.class, TagRowMapper.class})
public class WebAppConfig {
    @Bean
    @Profile("PROD")
    public DataSource postgres(@Value("#{ environment['postgres.url']}") String url,
                               @Value("#{ environment['postgres.user']}") String user,
                               @Value("#{ environment['postgres.password']}") String password) {
        BasicDataSource pooledDataSource = new BasicDataSource();
        pooledDataSource.setUrl(url);
        pooledDataSource.setUsername(user);
        pooledDataSource.setPassword(password);

        pooledDataSource.setInitialSize(5);
        pooledDataSource.setMaxTotal(10);
        pooledDataSource.setMaxWaitMillis(600);
        return pooledDataSource;
    }

    @Bean
    @Profile("PROD")
    public GiftCertRepository postgresRepo(DataSource postgres, GiftCertRowMapper giftCertRM, TagRowMapper tagRM){
        return new PostgresGiftCertsRepo(postgres, giftCertRM, tagRM);
    }


    @Bean
    @Profile("DEV")
    public DataSource h2(){
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("classpath:schema.sql")
                .addScript("classpath:data.sql").build();
    }

    @Bean
    @Profile("DEV")
    public GiftCertRepository H2Repo(DataSource h2, GiftCertRowMapper giftCertRM, TagRowMapper tagRM){
        return new H2GiftCertsRepo(h2, giftCertRM, tagRM);
    }


}
