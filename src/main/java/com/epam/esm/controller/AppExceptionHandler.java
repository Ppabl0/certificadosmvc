package com.epam.esm.controller;

import com.epam.esm.exceptions.*;
import com.epam.esm.exceptions.Error;
import com.epam.esm.model.GiftCert;
import com.epam.esm.model.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;

@ControllerAdvice
public class AppExceptionHandler {
    @ExceptionHandler(InvalidGiftCertCreationException.class)
    public ResponseEntity<Error> invalidGiftCertCreation(InvalidGiftCertCreationException ex){
        HashMap<String, String> atribs = new HashMap<>();
        GiftCert giftCert = ex.getGiftCert();
        Error error = new Error();
        error.setErrorCode("400.10");

        if (ex.getGiftCert() == null){
            error.setErrorMessage("missing request body");
        } else {

            atribs.put("Name", giftCert.getGiftCertName());
            atribs.put("Price", String.valueOf(giftCert.getPrice()));
            atribs.put("Duration", String.valueOf(giftCert.getDuration()));

            error.setErrorMessage("Cannot create resource, invalid or missing values, duration cannot have a value of 0");
            error.setDetails(atribs);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<Error> notFound(ResourceNotFoundException ex){
        HashMap<String, String> atribs = new HashMap<>();

        atribs.put("Id or Name", ex.getSearchValue());
        Error error = new Error();
        error.setErrorMessage("Resource not found with given id or name");
        error.setErrorCode("404.20");
        error.setDetails(atribs);

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
    }

    @ExceptionHandler(InvalidGiftCertSearchException.class)
    public ResponseEntity<Error> invalidgiftsearch(){
        Error error = new Error();
        error.setErrorMessage("Invalid search: no name, description, or tag to make search.");
        error.setErrorCode("400.20");

        HashMap<String, String> atribs = new HashMap<>();
        atribs.put("Name", null);
        atribs.put("Description", null);
        atribs.put("Tag", null);
        error.setDetails(atribs);

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

    @ExceptionHandler(NoMatchFoundException.class)
    public ResponseEntity<String> noMatch(){
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No match found");
    }

    @ExceptionHandler(InvalidModificationException.class)
    public ResponseEntity<Error> invalidUpdate(InvalidModificationException ex){
        Error error = new Error();
        error.setErrorMessage("Cannot modify resource, missing or invalid inputs");
        error.setErrorCode("400.30");
        HashMap<String, String> atribs = new HashMap<>();

        if (ex.getGiftCert() != null){
            GiftCert giftCert = ex.getGiftCert();
            atribs.put("Name", giftCert.getGiftCertName());
            atribs.put("Description", giftCert.getDescription());
            atribs.put("Price", String.valueOf(giftCert.getPrice()));
            atribs.put("Duration", String.valueOf(giftCert.getDuration()));
            error.setDetails(atribs);
        } else if (ex.getTag() != null) {
            Tag tag = ex.getTag();
            atribs.put("Name", tag.getTagName());
            error.setDetails(atribs);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

    @ExceptionHandler(InvalidTagCreationException.class)
    public ResponseEntity<Error> invalidTag(InvalidTagCreationException ex){
        Error error = new Error();
        error.setErrorMessage("Cannot create resource, Name is missing");
        error.setErrorCode("400.40");
        if (ex.getTag() == null){
            HashMap<String, String> atribs = new HashMap<>();
            atribs.put("Name", ex.getTag().getTagName());
            error.setDetails(atribs);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }
}
