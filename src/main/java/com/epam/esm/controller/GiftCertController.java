package com.epam.esm.controller;

import com.epam.esm.model.GiftCert;
import com.epam.esm.service.GiftCertService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/giftcerts")
public class GiftCertController {
    private final GiftCertService giftCertService;

    public GiftCertController(GiftCertService giftCertService){
        this.giftCertService = giftCertService;
    }


    /**
     *Creates new Gift certificate (GiftCert) with given tags and atributes.
     * if id, creation date or last update date are provided, will be ignored.
     * <b>Description</b> is optional, default value is "" (empty string)
     *
     * <ul>
     *     <li><b>Endpoint</b>: /giftcerts</li>
     *     <li><b>Method</b>: POST</li>
     *     <li>accepts: application/json</li>
     *     <li><b>Parameters</b>:
     *       <ul>
     *         <li>tags: muliple tags should be separated with ";" (One;Two)  OPTIONAL</li>
     *       </ul>
     *     </li>
     *     <li><b>Request Body</b>: JSON with following values:
     *       <ul>
     *           <li>giftCertName: required</li>
     *           <li>description: optional, default value = ""</li>
     *           <li>price: required, amount of money the certificate has</li>
     *           <li>duration: required, Integer, represents duration in days. Must not be 0</li>
     *       </ul>
     *     </li>
     * </ul>
     * <h4>Example of request body</h4>
     * <pre>
     *     {
     *         "giftCertName": "cars"
     *         "description": "description example"
     *         "price":300
     *         "duration":15
     *     }
     * </pre>
     *
     * @return the created GiftCert, and Location header with path to the resource.
     */
    @PostMapping
    public ResponseEntity<GiftCert> newGiftCert(@RequestParam(required = false) String tags,
                                                @RequestBody GiftCert giftCert,
                                                UriComponentsBuilder uriComponentsBuilder){

        GiftCert result = giftCertService.saveGiftCert(giftCert, tags);

        URI address = uriComponentsBuilder.path("/giftcerts/")
                        .path(String.valueOf(result.getId()))
                        .build()
                        .toUri();

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(address);

        return ResponseEntity.status(HttpStatus.CREATED)
                .headers(headers)
                .body(result);
    }

    /**
     * Search a GiftCert with a given id
     * <ul>
     *     <li><b>Endpoint</b>: /giftcerts/{id}</li>
     *     <li><b>Method</b>: GET</li>
     *     <li>accepts: application/json</li>
     *     <li><b>Path variable</b>: id, the id of a GiftCert to search</li>
     * </ul>
     * <h4>Example of request</h4>
     * <p>
     *     GET localhost/giftcerts/7
     * </p>
     *
     * @return The giftCert with given id if it exists
     */
    @GetMapping("/{id}")
    public ResponseEntity<GiftCert> getGiftCertById(@PathVariable int id){
        return ResponseEntity.status(HttpStatus.OK)
                .body(giftCertService.findGiftCertById(id));
    }

    /**
     * finds all gift certificates
     * <ul>
     *     <li><b>Endpoint</b>: /giftcerts</li>
     *     <li><b>Method</b>: GET</li>
     *     <li>accepts: application/json</li>
     * </ul>
     * <h4>Example of request</h4>
     * <p>
     *     GET localhost/giftcerts
     * </p>
     *
     * @return JSON representation of GiftCerts found, may be empty
     */
    @GetMapping
    public ResponseEntity<List<GiftCert>> getAllGiftCerts(){
        return ResponseEntity.status(HttpStatus.OK)
                .body(giftCertService.findAllGiftCert());
    }

    /**
     * Search gift certificates that match given name, decription or tag
     * params can be used in combination or individually.
     * Also supports sorting by name or creation date, woth ascending order or descending order
     * <ul>
     *     <li><b>Endpoint</b>: /giftcerts/search</li>
     *     <li><b>Method</b>: GET</li>
     *     <li><b>Parameters</b>:
     *       <ul>
     *         <li>name: optional, search giftCert with given value or any that has anywhere in its name this value </li>
     *         <li>description: optional, search giftCert with given description or any that has anywhere in its
     *             description this value</li>
     *         <li>tag: optional, search any giftCert that has this tag</li>
     *         <li>sortColumn: optional, valid values = name, date. Any other value won't sort the matches</li>
     *         <li>desc: optional, default value false. it can be true or false</li>
     *       </ul>
     *     </li>
     * </ul>
     * <h4>Example of request</h4>
     * <p>
     *     GET localhost/search?name=fruit&description=juice&tag=berry&sortColumn=date&desc=true
     * </p>
     * @return Gift certificates that matches the search as JSON object
     */
    @GetMapping("/search")
    public ResponseEntity<List<GiftCert>> search(@RequestParam(required = false) String name,
                                                 @RequestParam(required = false) String description,
                                                 @RequestParam(required = false) String tag,
                                                 @RequestParam(required = false) String sortColumn,
                                                 @RequestParam(required = false) boolean desc){

        return ResponseEntity.status(HttpStatus.OK)
                        .body(giftCertService.searchGiftCerts(name, description, tag, sortColumn, desc));
    }


    /**
     * Modifies a Gift certificate that matches id
     * <ul>
     *     <li><b>Endpoint</b>: /giftcerts/{id}</li>
     *     <li><b>Method</b>: PUT</li>
     *     <li>accepts: application/json</li>
     *     <li><b>Parameters</b>:
     *       <ul>
     *         <li>tags: optional, muliple tags should be separated with ";" (One;Two)</li>
     *       </ul>
     *     </li>
     *     <li><b>Request Body</b>: JSON with following values:
     *       <ul>
     *           <li>giftCertName: optional</li>
     *           <li>description: optional</li>
     *           <li>price: optional</li>
     *           <li>duration: optional</li>
     *           <li>* all values are optional but at least one must be provided to perform the modification</li>
     *       </ul>
     *     </li>
     * </ul>
     * <h4>Example of request body</h4>
     * <pre>
     *     {
     *         "giftCertName": "cars"
     *         "description": "description example"
     *         "price":300
     *         "duration":15
     *     }
     * </pre>
     *
     * @return the updated GiftCert
     */
    @PutMapping("/{id}")
    public ResponseEntity<GiftCert> modify(@PathVariable int id,
                                           @RequestBody GiftCert newData,
                                           @RequestParam(required = false) String tags){
        return ResponseEntity.status(HttpStatus.OK)
                .body(giftCertService.modifyGiftCert(id, newData, tags));
    }


    /**
     * Deletes Gift certificate with given id
     * <ul>
     *     <li><b>Endpoint</b>: /giftcerts/{id}</li>
     *     <li><b>Method</b>: DELETE</li>
     * </ul>
     * <h4>Example of request</h4>
     * <p>
     *     DELETE localhost/giftcerts/7
     * </p>
     *
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteGiftCert(@PathVariable int id){
        giftCertService.dropGiftCert(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body("");
    }

}
