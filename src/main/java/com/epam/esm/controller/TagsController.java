package com.epam.esm.controller;

import com.epam.esm.model.Tag;
import com.epam.esm.service.TagService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/tags")
public class TagsController {
    private final TagService tagService;
    TagsController(TagService tagService){
        this.tagService = tagService;
    }

    /**
     * Creates new Tag with given name, if id is provided, will be ignored, id is assigned automatically
     *
     * <ul>
     *     <li><b>Endpoint</b>: /tags</li>
     *     <li><b>Method</b>: POST</li>
     *     <li>accepts: application/json</li>
     *     <li><b>Request Body</b>: JSON with following values:
     *       <ul>
     *           <li>tagName: required</li>
     *       </ul>
     *     </li>
     * </ul>
     * <h4>Example of request body</h4>
     * <pre>
     *     {
     *         "tagName": "autumn"
     *     }
     * </pre>
     *
     * @return the created tag, and Location header with path to the resource.
     */
    @PostMapping
    public ResponseEntity<Tag> createTag(@RequestBody Tag tag, UriComponentsBuilder uriComponentsBuilder){
        Tag result = tagService.addNewTag(tag);
        URI address = uriComponentsBuilder.path("/tags/")
                .path(String.valueOf(result.getId()))
                .build()
                .toUri();

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(address);

        return ResponseEntity.status(HttpStatus.CREATED).headers(headers).body(result);
    }

    /**
     * Search tag with a given id
     * <ul>
     *     <li><b>Endpoint</b>: /tags/{id}</li>
     *     <li><b>Method</b>: GET</li>
     *     <li>accepts: application/json</li>
     *     <li><b>Path variable</b>: id, the tag's id to search</li>
     * </ul>
     * <h4>Example of request</h4>
     * <p>
     *     GET localhost/tags/7
     * </p>
     *
     * @return The tag with given id if it exists
     */
    @GetMapping("/{id}")
    public ResponseEntity<Tag> findbyId(@PathVariable int id){
        return ResponseEntity.status(HttpStatus.OK).body(tagService.findTagById(id));
    }

    /**
     * Search tag with a given name
     * <ul>
     *     <li><b>Endpoint</b>: /tags/name</li>
     *     <li><b>Method</b>: GET</li>
     *     <li>accepts: application/json</li>
     *     <li><b>Parameters</b>:
     *      *       <ul>
     *      *         <li>name: required, the tag name to search</li>
     *      *       </ul>
     *      *     </li>
     * </ul>
     * <h4>Example of request</h4>
     * <p>
     *     GET localhost/tags/name?name=summer
     * </p>
     *
     * @return The tag with given name
     */
    @GetMapping("/name")
    public ResponseEntity<Tag> findByName(@RequestParam String name){
        return ResponseEntity.status(HttpStatus.OK).body(tagService.findTagByName(name));
    }

    /**
     * gets all tags
     * <ul>
     *     <li><b>Endpoint</b>: /tagsli>
     *     <li><b>Method</b>: GET</li>
     *     <li>accepts: application/json</li>
     * </ul>
     * <h4>Example of request</h4>
     * <p>
     *     GET localhost/tags
     * </p>
     *
     * @return List of tags, may be empty
     */
    @GetMapping
    public ResponseEntity<List<Tag>> allTags(){
        return ResponseEntity.status(HttpStatus.OK).body(tagService.findAllTags());
    }

    /**
     * Modifies tag that matches given id
     * <ul>
     *     <li><b>Endpoint</b>: /tags/{id}</li>
     *     <li><b>Method</b>: PUT</li>
     *     <li>accepts: application/json</li>
     *     <li><b>Request Body</b>: JSON with following values:
     *       <ul>
     *           <li>tagName: required, the only field that can be modified</li>
     *       </ul>
     *     </li>
     * </ul>
     * <h4>Example of request body</h4>
     * <pre>
     *     {
     *         "tagName": "winter"
     *     }
     * </pre>
     *
     * @return the updated tag
     */
    @PutMapping("/{id}")
    public ResponseEntity<Tag> modify(@PathVariable int id, @RequestBody Tag tag){
        return ResponseEntity.status(HttpStatus.OK).body(tagService.modifyTag(id, tag));
    }

    /**
     * Deletes tag with given id
     * <ul>
     *     <li><b>Endpoint</b>: /tags/{id}</li>
     *     <li><b>Method</b>: DELETE</li>
     * </ul>
     * <h4>Example of request</h4>
     * <p>
     *     DELETE localhost/tags/7
     * </p>
     *
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<String> drop(@PathVariable int id){
        tagService.dropTag(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body("");

    }
}
