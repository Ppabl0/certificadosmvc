package com.epam.esm.exceptions;

import com.epam.esm.model.GiftCert;

public class InvalidGiftCertCreationException extends RuntimeException{

    private GiftCert giftCert;

    public InvalidGiftCertCreationException(GiftCert giftCert){
        this.giftCert = giftCert;
    }

    public GiftCert getGiftCert() {
        return giftCert;
    }
}
