package com.epam.esm.exceptions;

import com.epam.esm.model.GiftCert;
import com.epam.esm.model.Tag;

public class InvalidModificationException extends RuntimeException{

    private GiftCert giftCert;
    private Tag tag;

    public InvalidModificationException(GiftCert giftCert){
        this.giftCert = giftCert;
    }

    public InvalidModificationException(Tag tag){
        this.tag = tag;
    }

    public GiftCert getGiftCert() {
        return giftCert;
    }

    public Tag getTag() {
        return tag;
    }
}
