package com.epam.esm.exceptions;

import com.epam.esm.model.Tag;

public class InvalidTagCreationException extends RuntimeException{
    Tag tag;

    public InvalidTagCreationException(Tag tag){
        this.tag = tag;
    }

    public Tag getTag() {
        return tag;
    }
}
