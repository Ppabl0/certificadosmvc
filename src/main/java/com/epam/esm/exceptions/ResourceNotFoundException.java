package com.epam.esm.exceptions;

public class ResourceNotFoundException extends RuntimeException{
    private String searchValue;

    public ResourceNotFoundException(String identifier){
        this.searchValue = identifier;
    }

    public String getSearchValue() {
        return searchValue;
    }
}
