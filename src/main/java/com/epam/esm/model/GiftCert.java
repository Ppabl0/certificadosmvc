package com.epam.esm.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class GiftCert {
    private int id;
    private String giftCertName;
    private String description;
    private BigDecimal price;
    private Integer duration;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss[.SSSSSS]")
    private LocalDateTime createDate;
    @JsonFormat(pattern = "yyy-MM-dd HH:mm:ss[.SSSSSS]")
    private LocalDateTime lastUpdateDate;
    private List<Tag> tags;

    public GiftCert(){}

    public GiftCert(int id, String giftName, String description, BigDecimal price, Integer duration, LocalDateTime createDate, LocalDateTime lastUpdateDate, List<Tag> tags) {
        this.id = id;
        this.giftCertName = giftName;
        this.description = description;
        this.price = price;
        this.duration = duration;
        this.createDate = createDate;
        this.lastUpdateDate = lastUpdateDate;
        this.tags = tags;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setGiftCertName(String giftCertName) {
        this.giftCertName = giftCertName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public void setLastUpdateDate(LocalDateTime lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public int getId() {
        return id;
    }

    public String getGiftCertName() {
        return giftCertName;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Integer getDuration() {
        return duration;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public LocalDateTime getLastUpdateDate() {
        return lastUpdateDate;
    }

    public List<Tag> getTags() {
        return tags;
    }

    @Override
    public String toString() {
        return "GiftCert{" +
                "id=" + id +
                ", giftCertName='" + giftCertName + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", duration=" + duration +
                ", createDate='" + createDate + '\'' +
                ", lastUpdateDate='" + lastUpdateDate + '\'' +
                ", tags=" + tags +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GiftCert giftCert = (GiftCert) o;
        return id == giftCert.id && Objects.equals(giftCertName, giftCert.giftCertName) && Objects.equals(description, giftCert.description) && Objects.equals(price, giftCert.price) && Objects.equals(duration, giftCert.duration) && Objects.equals(createDate, giftCert.createDate) && Objects.equals(lastUpdateDate, giftCert.lastUpdateDate) && Objects.equals(tags, giftCert.tags);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, giftCertName, description, price, duration, createDate, lastUpdateDate, tags);
    }
}
