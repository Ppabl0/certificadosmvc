package com.epam.esm.repository;

import com.epam.esm.model.GiftCert;
import com.epam.esm.model.Tag;

import java.util.List;

public interface GiftCertRepository {
    GiftCert getGiftCertById(int id);
    List<GiftCert> getAllGiftCert();
    int addGiftCert(GiftCert giftCert);
    List<GiftCert> searchGiftCertByNameDescriptionTag(String name, String description, Integer tag,
                                                      String sortColumn, boolean desc);
    GiftCert updateGiftCert(int id, GiftCert newData);
    int deleteGiftCert(int id);
    GiftCert findGiftCertByName(String name );


    int createNewTag(String name);
    Tag getTagById(int id);
    Tag getTagByName(String tagName);
    List<Tag> getTagsByGiftCertId(int id);
    List<Tag> getAllTags();
    int updateTag(int id, String name);
    int deleteTag(int id);


    void addTagToGiftCert(int giftCertId, int tagId);

}
