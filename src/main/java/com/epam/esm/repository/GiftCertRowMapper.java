package com.epam.esm.repository;

import com.epam.esm.model.GiftCert;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class GiftCertRowMapper implements RowMapper<GiftCert> {
    @Override
    public GiftCert mapRow(ResultSet rs, int rowNum) throws SQLException {
        GiftCert giftCert = new GiftCert();
        giftCert.setId(rs.getInt(1));
        giftCert.setGiftCertName(rs.getString(2));
        giftCert.setDescription(rs.getString(3));
        giftCert.setPrice(rs.getBigDecimal(4));
        giftCert.setDuration(rs.getInt(5));
        giftCert.setCreateDate(rs.getTimestamp(6).toLocalDateTime());
        giftCert.setLastUpdateDate(rs.getTimestamp(7).toLocalDateTime());
        return giftCert;
    }
}
