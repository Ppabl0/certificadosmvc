package com.epam.esm.repository;

import com.epam.esm.model.GiftCert;
import com.epam.esm.model.Tag;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;
import java.util.Locale;

public class H2GiftCertsRepo implements GiftCertRepository {
    private final JdbcTemplate jdbcTemplate;
    private final GiftCertRowMapper giftCertRowMapper;
    private final TagRowMapper tagRowMapper;

    public H2GiftCertsRepo(DataSource h2DataSource, GiftCertRowMapper giftCertRowMapper, TagRowMapper tagRowMapper){
        this.jdbcTemplate = new JdbcTemplate(h2DataSource);
        this.giftCertRowMapper = giftCertRowMapper;
        this.tagRowMapper = tagRowMapper;
    }


    @Override
    public int addGiftCert(GiftCert giftCert){
        String description = giftCert.getDescription();
        description = description == null ? "" : description;

        String h2SqlQuery = "INSERT INTO GIFTCERTS (GIFTNAME, DESCRIPTION, PRICE, DURATION) VALUES (?, ?, ?, ?);";
        return jdbcTemplate.update(h2SqlQuery,
                                   giftCert.getGiftCertName(),
                                   description,
                                   giftCert.getPrice(),
                                   giftCert.getDuration());
    }

    @Override
    public GiftCert getGiftCertById(int giftCertId){
        String h2SqlQuery = "SELECT * FROM giftcerts WHERE id = ?;";

        List<GiftCert> result = jdbcTemplate.query(h2SqlQuery, giftCertRowMapper, giftCertId);

        if (result.size() == 0){
            return null;
        } else return result.get(0);
    }

    @Override
    public GiftCert findGiftCertByName(String name) {
        String h2SqlQuery = "SELECT * FROM giftcerts WHERE giftname = ?;";

        return jdbcTemplate.queryForObject(h2SqlQuery, giftCertRowMapper, name);
    }

    @Override
    public List<GiftCert> searchGiftCertByNameDescriptionTag(String name, String description, Integer tagId,
                                                             String sortColumn, boolean isDesc){
        StringBuilder h2SqlQuery;
        String sortOrder = isDesc ? "DESC" : "";

        if (tagId == null){
            h2SqlQuery = new StringBuilder("SELECT * FROM giftcerts WHERE ");
            addNameAndOrDescriptionToQuery(h2SqlQuery, name, description);
        } else {
            h2SqlQuery = new StringBuilder("SELECT * FROM giftcerts JOIN giftcert_tag ON id=gift_id WHERE tag_id=").append(tagId);

            if (name != null || description != null) {
                h2SqlQuery.append(" AND ");
                addNameAndOrDescriptionToQuery(h2SqlQuery, name, description);
            }
        }

        addSortingColumnAndOrder(h2SqlQuery, sortColumn, sortOrder);


        return jdbcTemplate.query(h2SqlQuery.toString(), giftCertRowMapper);
    }

    @Override
    public GiftCert updateGiftCert(int id, GiftCert newData) {
        StringBuilder h2SqlQuery = new StringBuilder("UPDATE giftcerts SET ");

        if (newData.getGiftCertName() != null){
            h2SqlQuery.append("giftname = '").append(newData.getGiftCertName()).append("', ");
        }
        if (newData.getDescription() != null){
            h2SqlQuery.append("description = '").append(newData.getDescription()).append("', ");
        }
        if (newData.getPrice() != null){
            h2SqlQuery.append("price = ").append(newData.getPrice()).append(", ");
        }
        if (newData.getDuration() != null){
            h2SqlQuery.append("duration = ").append(newData.getDuration()).append(", ");
        }

        h2SqlQuery.deleteCharAt(h2SqlQuery.lastIndexOf(","));

        h2SqlQuery.append("WHERE id = ?;");

        jdbcTemplate.update(h2SqlQuery.toString(), id);
        return getGiftCertById(id);
    }

    @Override
    public int deleteGiftCert(int id){
        String h2SqlQuery = "DELETE FROM giftcerts WHERE id = ?";
        return jdbcTemplate.update(h2SqlQuery, id);
    }

    @Override
    public List<GiftCert> getAllGiftCert(){
        String sqlQuery = "SELECT * FROM GIFTCERTS";
        return jdbcTemplate.query(sqlQuery, giftCertRowMapper);
    }



    @Override
    public int createNewTag(String tagName) {
        String h2SqlQuery = "INSERT INTO tags (name) VALUES (?);";

        return jdbcTemplate.update(h2SqlQuery, tagName.toUpperCase(Locale.ROOT));
    }

    @Override
    public Tag getTagById(int id){
        String h2SqlQuery = "SELECT * FROM tags WHERE id = ?";
        List<Tag> tags = jdbcTemplate.query(h2SqlQuery, tagRowMapper, id);
        return tags.size() == 0 ? null : tags.get(0);
    }

    @Override
    public Tag getTagByName(String tagName){ //individual
        String h2SqlQuery = "SELECT * FROM tags WHERE name = UPPER(?);";

        List<Tag> tags = jdbcTemplate.query(h2SqlQuery, tagRowMapper, tagName);
        return tags.size() == 0 ? null : tags.get(0);

    }

    @Override
    public List<Tag> getAllTags(){
        String h2SqlQuery = "SELECT * FROM tags;";

        return jdbcTemplate.query(h2SqlQuery, tagRowMapper);
    }

    @Override
    public int updateTag(int id, String name){
        String h2SqlQuery = "UPDATE tags SET name = ? WHERE id = ?";
        return jdbcTemplate.update(h2SqlQuery, name, id);
    }

    @Override
    public int deleteTag(int id){
        String h2SqlQuery = "DELETE FROM tags WHERE id = ?";
        return jdbcTemplate.update(h2SqlQuery, id);
    }


    @Override
    public List<Tag> getTagsByGiftCertId(int giftCertId){
        String h2SqlQuery = "SELECT id, name FROM tags JOIN giftcert_tag ON tag_id = id WHERE gift_id = ?";

        return jdbcTemplate.query(h2SqlQuery, tagRowMapper, giftCertId);
    }


    @Override
    public void addTagToGiftCert(int giftCertId, int tagId) {
        String h2SqlQuery = "INSERT INTO giftcert_tag (gift_id, tag_id) VALUES (?, ?);";
        jdbcTemplate.update(h2SqlQuery, giftCertId, tagId);
    }



    //HELPER METHODS
    private void addNameAndOrDescriptionToQuery(StringBuilder query, String name, String description){
        //add name and/or description condition
        if (name != null && description != null){
            query.append("giftname ILIKE '%").append(name)
                    .append("%' AND description ILIKE '%").append(description)
                    .append("%' ");
        } else if (description == null) {
            query.append("giftname ILIKE '%").append(name).append("%' ");
        } else {
            query.append("description ILIKE '%").append(description).append("%' ");
        }
    }

    private void addSortingColumnAndOrder(StringBuilder query, String sortColumn, String sortOrder){
        //add sorting
        if (sortColumn.equalsIgnoreCase("name")){
            query.append("ORDER BY giftname ").append(sortOrder).append(", create_date");
        } else if (sortColumn.equalsIgnoreCase("date")){
            query.append("ORDER BY create_date ").append(sortOrder).append(", giftname");
        }
    }
}
