package com.epam.esm.repository;

import com.epam.esm.model.GiftCert;
import com.epam.esm.model.Tag;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;
import java.util.Locale;


public class PostgresGiftCertsRepo implements GiftCertRepository {
    private final JdbcTemplate jdbcTemplate;
    private final GiftCertRowMapper giftCertRowMapper;
    private final TagRowMapper tagRowMapper;

    public PostgresGiftCertsRepo(DataSource dataSource, GiftCertRowMapper giftCertRowMapper, TagRowMapper tagRowMapper){
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.giftCertRowMapper = giftCertRowMapper;
        this.tagRowMapper = tagRowMapper;
    }

    @Override
    public int addGiftCert(GiftCert giftCert){
        String description = giftCert.getDescription();
        description = description == null ? "" : description;

        String sqlQuery = "INSERT INTO giftcerts (giftname, description, price, duration) VALUES (?, ?, ?, ?);";
        return jdbcTemplate.update(sqlQuery,
                giftCert.getGiftCertName(),
                description,
                giftCert.getPrice(),
                giftCert.getDuration());
    }

    @Override
    public GiftCert getGiftCertById(int giftCertId){
        String sqlQuery = "SELECT * FROM giftcerts WHERE id = ?;";

        List<GiftCert> result = jdbcTemplate.query(sqlQuery, giftCertRowMapper, giftCertId);

        if (result.size() == 0){
            return null;
        } else return result.get(0);
    }

    @Override
    public GiftCert findGiftCertByName(String name) { //INdividual
        String sqlQuery = "SELECT * FROM giftcerts WHERE giftname = ?;";

        return jdbcTemplate.queryForObject(sqlQuery, giftCertRowMapper, name);
    }

    @Override
    public List<GiftCert> searchGiftCertByNameDescriptionTag(String name, String description, Integer tagId,
                                                             String sortColumn, boolean isDesc){
        StringBuilder sqlQuery;
        String sortOrder = isDesc ? "DESC" : "";

        if (tagId == null){
            sqlQuery = new StringBuilder("SELECT * FROM giftcerts WHERE ");
            addNameAndOrDescriptionToQuery(sqlQuery, name, description);
        } else {
            sqlQuery = new StringBuilder("SELECT * FROM giftcerts JOIN giftcert_tag ON id = gift_id WHERE tag_id =")
                                          .append(tagId);

            if (name != null || description != null) {
                sqlQuery.append(" AND ");
                addNameAndOrDescriptionToQuery(sqlQuery, name, description);
            }
        }

        addSortingColumnAndOrder(sqlQuery, sortColumn, sortOrder);


        return jdbcTemplate.query(sqlQuery.toString(), giftCertRowMapper);
    }


    @Override
    public GiftCert updateGiftCert(int id, GiftCert newData) {
        StringBuilder sqlQuery = new StringBuilder("UPDATE giftcerts SET ");

        if (newData.getGiftCertName() != null){
            sqlQuery.append("giftname = '").append(newData.getGiftCertName()).append("', ");
        }
        if (newData.getDescription() != null){
            sqlQuery.append("description = '").append(newData.getDescription()).append("', ");
        }
        if (newData.getPrice() != null){
            sqlQuery.append("price = ").append(newData.getPrice()).append(", ");
        }
        if (newData.getDuration() != null){
            sqlQuery.append("duration = ").append(newData.getDuration()).append(", ");
        }

        sqlQuery.deleteCharAt(sqlQuery.lastIndexOf(","));

        sqlQuery.append("WHERE id = ?;");

        jdbcTemplate.update(sqlQuery.toString(), id);
        return getGiftCertById(id);
    }

    @Override
    public int deleteGiftCert(int id){
        String sqlQuery = "DELETE FROM giftcerts WHERE id = ?";
        return jdbcTemplate.update(sqlQuery, id);
    }


    @Override
    public List<GiftCert> getAllGiftCert(){
        String sqlQuery = "SELECT * FROM GIFTCERTS";

        return jdbcTemplate.query(sqlQuery, giftCertRowMapper);
    }


    @Override
    public int createNewTag(String tagName) {
        String sqlQuery = "INSERT INTO tags (name) VALUES (?);";
        return jdbcTemplate.update(sqlQuery, tagName.toUpperCase(Locale.ROOT));
    }

    @Override
    public Tag getTagById(int id){
        String sqlQuery = "SELECT * FROM tags WHERE id = ?";
        List<Tag> tags = jdbcTemplate.query(sqlQuery, tagRowMapper, id);
        return tags.size() == 0 ? null : tags.get(0);
    }

    @Override
    public Tag getTagByName(String tagName){ //individual
        String sqlQuery = "SELECT * FROM tags WHERE name = UPPER(?);";

        List<Tag> tags = jdbcTemplate.query(sqlQuery, tagRowMapper, tagName);
        return tags.size() == 0 ? null : tags.get(0);
    }

    @Override
    public List<Tag> getAllTags(){
        String sqlQuery = "SELECT * FROM tags;";

        return jdbcTemplate.query(sqlQuery, tagRowMapper);
    }

    @Override
    public int updateTag(int id, String name){
        String sqlQuery = "UPDATE tags SET name = ? WHERE id = ?";
        return jdbcTemplate.update(sqlQuery, name, id);
    }

    @Override
    public int deleteTag(int id){
        String sqlQuery = "DELETE FROM tags WHERE id = ?";
        return jdbcTemplate.update(sqlQuery, id);
    }

    @Override
    public List<Tag> getTagsByGiftCertId(int giftCertId){
        String sqlQuery = "SELECT id, name FROM tags JOIN giftcert_tag ON tag_id = id WHERE gift_id = ?";

        return jdbcTemplate.query(sqlQuery, tagRowMapper, giftCertId);
    }


    @Override
    public void addTagToGiftCert(int giftCertId, int tagId) {
        String sqlQuery = "INSERT INTO giftcert_tag (gift_id, tag_id) VALUES (?, ?);";
        jdbcTemplate.update(sqlQuery, giftCertId, tagId);
    }



    //HELPER METHODS
    private void addNameAndOrDescriptionToQuery(StringBuilder query, String name, String description){
        //add name and/or description condition
        if (name != null && description != null){
            query.append("giftname ILIKE '%").append(name)
                    .append("%' AND description ILIKE '%").append(description)
                    .append("%' ");
        } else if (description == null) {
            query.append("giftname ILIKE '%").append(name).append("%' ");
        } else {
            query.append("description ILIKE '%").append(description).append("%' ");
        }
    }

    private void addSortingColumnAndOrder(StringBuilder query, String sortColumn, String sortOrder){
        //add sorting
        if (sortColumn.equalsIgnoreCase("name")){
            query.append("ORDER BY giftname ").append(sortOrder).append(", create_date");
        } else if (sortColumn.equalsIgnoreCase("date")){
            query.append("ORDER BY create_date ").append(sortOrder).append(", giftname");
        }
    }
}
