package com.epam.esm.service;

import com.epam.esm.exceptions.*;
import com.epam.esm.model.GiftCert;
import com.epam.esm.model.Tag;
import com.epam.esm.repository.GiftCertRepository;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class GiftCertService {
    private final GiftCertRepository giftCertRepository;

    public GiftCertService(GiftCertRepository giftCertRepository){
        this.giftCertRepository = giftCertRepository;
    }

    public GiftCert saveGiftCert(GiftCert giftCert, String tags){
        if (giftCert.getGiftCertName()==null || giftCert.getPrice()==null || giftCert.getDuration() == null || giftCert.getDuration() == 0){ //al crear nuevo la duration no debe ser 0 osea le default
            throw new InvalidGiftCertCreationException(giftCert);
        }

        GiftCert createdGiftCert = addAndGetGiftCert(giftCert);

        if (tags != null){
            String[] tagsArray = tags.split(";");
            addNewTagsToDB(tagsArray);
            createRelationTagsGiftCert(createdGiftCert.getId(), tagsArray);
            List<Tag> tagList = giftCertRepository.getTagsByGiftCertId(createdGiftCert.getId());
            createdGiftCert.setTags(tagList);
        }

        return createdGiftCert;
    }

    public GiftCert findGiftCertById(int id){
        GiftCert giftCert = giftCertRepository.getGiftCertById(id);
        if (giftCert != null){
            List<Tag> tags = giftCertRepository.getTagsByGiftCertId(giftCert.getId());
            giftCert.setTags(tags);
            return giftCert;
        } else {
            throw new ResourceNotFoundException(String.valueOf(id));
        }
    }

    public List<GiftCert> findAllGiftCert(){
        List<GiftCert> allGiftCert = giftCertRepository.getAllGiftCert();

        if (allGiftCert.size() > 0) {
            allGiftCert.forEach(giftCert -> {
                int id = giftCert.getId();
                giftCert.setTags(giftCertRepository.getTagsByGiftCertId(id));
            });
        }

        return allGiftCert;
    }

    public List<GiftCert> searchGiftCerts(String name, String description, String tag, String sortColumn, boolean desc){
        if (name == null && description == null && tag == null) {
            throw new InvalidGiftCertSearchException();
        }

        Integer tagId = ifTagIsValidAndExistsGetIdElseNull(tag);
        sortColumn = sortColumn == null ? "" : sortColumn;
        List<GiftCert> searchResult = giftCertRepository.searchGiftCertByNameDescriptionTag(name, description, tagId, sortColumn, desc);

        if (searchResult.size() > 0) {
            searchResult.forEach(giftCert -> {
                int id = giftCert.getId();
                giftCert.setTags(giftCertRepository.getTagsByGiftCertId(id));
            });
            return searchResult;
        } else {
            throw new NoMatchFoundException();
        }


    }

    public GiftCert modifyGiftCert(int id, GiftCert newData, String tags){
        if(hasValidValues(newData)){
            throw new InvalidModificationException(newData);
        }

        GiftCert updatedRecord = giftCertRepository.updateGiftCert(id, newData);

        if (updatedRecord == null){
            throw new ResourceNotFoundException(String.valueOf(id));
        }

        if (tags != null) {
            String[] tagsArray = tags.split(";");
            addNewTagsToDB(tagsArray);
            createRelationTagsGiftCert(updatedRecord.getId(), tagsArray);
        }

        List<Tag> tagList = giftCertRepository.getTagsByGiftCertId(updatedRecord.getId());

        updatedRecord.setTags(tagList);
        return updatedRecord;
    }

    public int dropGiftCert(int id){
        int result = giftCertRepository.deleteGiftCert(id);

        if ( result == 0){
            throw new ResourceNotFoundException(String.valueOf(id));
        } else {
            return result;
        }
    }


    //HELPERS------------------------------------
    private void addNewTagsToDB(String[] tags){
        Arrays.stream(tags)
              .filter( tag -> giftCertRepository.getTagByName(tag) == null)
              .forEach(giftCertRepository::createNewTag);
    }

    private GiftCert addAndGetGiftCert(GiftCert giftCert){
        giftCertRepository.addGiftCert(giftCert);
        return giftCertRepository.findGiftCertByName(giftCert.getGiftCertName());
    }

    private void createRelationTagsGiftCert(int giftCertId, String[] tags){
        Arrays.stream(tags)
                .map(giftCertRepository::getTagByName)
                .mapToInt(Tag::getId)
                .forEach(tagId -> giftCertRepository.addTagToGiftCert(giftCertId, tagId));
    }

    private Integer ifTagIsValidAndExistsGetIdElseNull(String tag){

        if (tag != null) {
            Tag existingTag = giftCertRepository.getTagByName(tag);
            if (existingTag == null) {
                throw new NoMatchFoundException();
            } else {
                return existingTag.getId();
            }
        } else {
            return null;
        }
    }

    private boolean hasValidValues(GiftCert newData){
        if (newData == null){
            return false;
        }
        return newData.getGiftCertName() == null && newData.getDescription() == null
                && newData.getPrice() == null && newData.getDuration() == null;
    }

}
