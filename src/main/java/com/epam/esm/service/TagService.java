package com.epam.esm.service;

import com.epam.esm.exceptions.InvalidModificationException;
import com.epam.esm.exceptions.InvalidTagCreationException;
import com.epam.esm.exceptions.NoMatchFoundException;
import com.epam.esm.exceptions.ResourceNotFoundException;
import com.epam.esm.model.Tag;
import com.epam.esm.repository.GiftCertRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagService {
    private final GiftCertRepository repository;
    public TagService(GiftCertRepository repository){
        this.repository = repository;
    }

    public Tag addNewTag(Tag tag){
        if (tag != null && tag.getTagName() != null) {
            repository.createNewTag(tag.getTagName());
            return repository.getTagByName(tag.getTagName());
        } else {
            throw new InvalidTagCreationException(tag);
        }
    }

    public Tag findTagById(int id){
        Tag tag = repository.getTagById(id);
        if (tag == null){
            throw new ResourceNotFoundException(String.valueOf(id));
        } else {
            return tag;
        }
    }

    public Tag findTagByName(String name){
        Tag tag = repository.getTagByName(name);
        if (tag == null){
            throw new ResourceNotFoundException(name);
        } else {
            return tag;
        }
    }

    public List<Tag> findAllTags(){
        List<Tag> tags = repository.getAllTags();
        if (tags.size() == 0){
            throw new NoMatchFoundException();
        } else {
            return tags;
        }
    }

    public Tag modifyTag(int id, Tag tag){
        if (tag != null && tag.getTagName() != null) {
            int result = repository.updateTag(id, tag.getTagName().toUpperCase());

            if (result != 0) {
                return repository.getTagByName(tag.getTagName());
            } else {
                throw new ResourceNotFoundException(String.valueOf(id));
            }
        } else {
            throw new InvalidModificationException(tag);
        }
    }

    public int dropTag(int id){
        int result = repository.deleteTag(id);
        if (result != 0){
            return result;
        } else {
            throw new ResourceNotFoundException(String.valueOf(id));
        }
    }
}
