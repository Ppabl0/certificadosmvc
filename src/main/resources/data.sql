-- DML

INSERT INTO GIFTCERTS (GIFTNAME, DESCRIPTION, PRICE, DURATION)
    VALUES ('Ropa caballeros', 'canjeable en ropa para caballeros', 500, 30), --1
           ('Pantallas', 'canjeable en pantallas del departamento de electronica', 1500, 60), --2
           ('Videojuegos', 'canjeable en videojuegos, consolas, mandos del dpto videojuegos', 1000, 60), --3
           ('Devolucion de articulo', 'devolucion en tarjeta de regalo canjeable en cualquier dpto', 975.5, 365), --4
           ('Ropa dama dia de la madre', 'regalo por día de las madres canjeable ne ropa de dama', 1000, 60) --5
           ;

INSERT INTO TAGS (NAME)
    VALUES ('DIA DE LA MADRE'), --1
           ('DIA DEL PADRE'), --2
           ('DEVOLUCION'), --3
           ('VIDEOJUEGOS'), --4
           ('ELECTRONICA'), --5
           ('ROPA DAMA'), --6
           ('ROPA CABALLERO') --7
           ;

INSERT INTO GIFTCERT_TAG (GIFT_ID, TAG_ID)
    VALUES (1, 7),
           (2, 5),
           (3, 4),
           (4, 3),
           (4, 4),
           (4, 5),
           (4, 6),
           (4, 7),
           (5, 1),
           (5, 6)
           ;

COMMIT;