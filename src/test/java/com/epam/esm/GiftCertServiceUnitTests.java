package com.epam.esm;

import com.epam.esm.exceptions.*;
import com.epam.esm.model.GiftCert;
import com.epam.esm.model.Tag;
import com.epam.esm.repository.GiftCertRepository;
import com.epam.esm.service.GiftCertService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GiftCertServiceUnitTests {
    private GiftCertRepository giftCertsRepository;
    private GiftCertService giftCertService;

    @BeforeEach
    void setupService(){
        giftCertsRepository = mock(GiftCertRepository.class);
        giftCertService = new GiftCertService(giftCertsRepository);
    }

    @Test
    void findByIdWithValidIdNullTagsTest(){
        GiftCert x = new GiftCert(1, "Clothing", "valid for clothing", BigDecimal.valueOf(1000), 30,
                LocalDateTime.now(), LocalDateTime.now(), null);


        when(giftCertsRepository.getGiftCertById(1)).thenReturn(x);
        when(giftCertsRepository.getTagsByGiftCertId(1)).thenReturn(null);

        GiftCert result = giftCertService.findGiftCertById(1);

        assertEquals(result, x, "los certs no son iguales hay algun field incorreto");
    }

    @Test
    void findByIdWithValidIdWithTagsTest(){
        GiftCert x = new GiftCert();
        x.setId(2);
        x.setGiftCertName("Winter Clothing");
        x.setDescription("valid for clothing");
        x.setPrice(BigDecimal.valueOf(1000));
        x.setDuration(30);
        x.setCreateDate(LocalDateTime.now());
        x.setLastUpdateDate(LocalDateTime.now());
        ArrayList<Tag> tags = new ArrayList<>();
        tags.add(new Tag(1, "CLOTHING"));
        tags.add(new Tag(2, "ELECTRONIC"));
        x.setTags(tags);


        when(giftCertsRepository.getGiftCertById(2)).thenReturn(x);
        when(giftCertsRepository.getTagsByGiftCertId(2)).thenReturn(tags);

        GiftCert result = giftCertService.findGiftCertById(2);

        assertEquals(result, x, "los certs no son iguales hay algun field incorreto");
    }

    @Test
    void findByIdWithNullDescriptionTest(){
        GiftCert x = new GiftCert();
        x.setId(3);
        x.setGiftCertName("Winter Clothing");
        x.setPrice(BigDecimal.valueOf(1000));
        x.setDuration(30);
        x.setCreateDate(LocalDateTime.now());
        x.setLastUpdateDate(LocalDateTime.now());
        ArrayList<Tag> tags = new ArrayList<>();
        tags.add(new Tag(1, "CLOTHING"));
        tags.add(new Tag(2, "ELECTRONIC"));
        x.setTags(tags);


        when(giftCertsRepository.getGiftCertById(3)).thenReturn(x);
        when(giftCertsRepository.getTagsByGiftCertId(3)).thenReturn(tags);

        GiftCert result = giftCertService.findGiftCertById(3);

        assertEquals(result, x, "los certs no son iguales hay algun field incorreto");
    }

    @Test
    void findbyIdWithInvalidIdTest(){
        when(giftCertsRepository.getGiftCertById(0)).thenReturn(null);
        assertThrows(ResourceNotFoundException.class, () ->
                giftCertService.findGiftCertById(0), "should throw exception");
    }

    @Test
    void saveValidGiftCertTest(){
        GiftCert expected = new GiftCert();
        expected.setId(4);
        expected.setGiftCertName("Summer Clothing");
        expected.setDescription("valid for clothing");
        expected.setPrice(BigDecimal.valueOf(500.5));
        expected.setDuration(60);
        expected.setCreateDate(LocalDateTime.now());
        expected.setLastUpdateDate(LocalDateTime.now());
        ArrayList<Tag> tags = new ArrayList<>();
        tags.add(new Tag(1, "CLOTHING"));
        tags.add(new Tag(2, "SUMMER"));
        expected.setTags(tags);

        when(giftCertsRepository.findGiftCertByName("Summer Clothing")).thenReturn(expected);
        when(giftCertsRepository.getTagsByGiftCertId(4)).thenReturn(tags);
        when(giftCertsRepository.getTagByName("CLOTHING")).thenReturn(new Tag(1, "CLOTHING"));
        when(giftCertsRepository.getTagByName("SUMMER")).thenReturn(new Tag(2, "SUMMER"));

        GiftCert input = new GiftCert();
        input.setGiftCertName("Summer Clothing");
        input.setDescription("valid for clothing");
        input.setPrice(BigDecimal.valueOf(500.5));
        input.setDuration(60);

        GiftCert result = giftCertService.saveGiftCert(input, "CLOTHING;SUMMER");

        assertEquals(result, expected, "atributes should be same");

        //save without description
        input.setDescription(null);

        result = giftCertService.saveGiftCert(input, "CLOTHING;SUMMER");
        assertEquals(result, expected, "atributes should be same");

    }

    @Test
    void saveValidGiftCertWithNoTagsTest(){
        GiftCert expected = new GiftCert();
        expected.setId(4);
        expected.setGiftCertName("Summer Clothing");
        expected.setDescription("valid for clothing");
        expected.setPrice(BigDecimal.valueOf(500.5));
        expected.setDuration(60);
        expected.setCreateDate(LocalDateTime.now());
        expected.setLastUpdateDate(LocalDateTime.now());


        when(giftCertsRepository.findGiftCertByName("Summer Clothing")).thenReturn(expected);

        GiftCert input = new GiftCert();
        input.setGiftCertName("Summer Clothing");
        input.setDescription("valid for clothing");
        input.setPrice(BigDecimal.valueOf(500.5));
        input.setDuration(60);

        GiftCert result = giftCertService.saveGiftCert(input, null);

        assertEquals(result, expected, "atributes should be same");

        //save without description
        input.setDescription(null);

        result = giftCertService.saveGiftCert(input, null);
        assertEquals(result, expected, "atributes should be same");

        //ignore id, dates
        input.setId(72);
        input.setCreateDate(LocalDateTime.MAX);
        input.setLastUpdateDate(LocalDateTime.MIN);

        result = giftCertService.saveGiftCert(input, null);
        assertEquals(result, expected, "atributes should be same");

    }

    @Test
    void saveWithInvalidGiftCertShouldThrowExceptionTest(){
        GiftCert input = new GiftCert();

        //all null values
        assertThrows(InvalidGiftCertCreationException.class, ()-> giftCertService.saveGiftCert(input, null));
        assertThrows(InvalidGiftCertCreationException.class, ()-> giftCertService.saveGiftCert(input, "COMPUTERS"));

        //missing 2 values
        input.setGiftCertName("name");
        assertThrows(InvalidGiftCertCreationException.class, ()-> giftCertService.saveGiftCert(input, null));
        assertThrows(InvalidGiftCertCreationException.class, ()-> giftCertService.saveGiftCert(input, "COMPUTERS"));

        input.setGiftCertName(null);
        input.setPrice(BigDecimal.valueOf(1));
        assertThrows(InvalidGiftCertCreationException.class, ()-> giftCertService.saveGiftCert(input, null));
        assertThrows(InvalidGiftCertCreationException.class, ()-> giftCertService.saveGiftCert(input, "COMPUTERS"));

        input.setPrice(null);
        input.setDuration(1);
        assertThrows(InvalidGiftCertCreationException.class, ()-> giftCertService.saveGiftCert(input, null));
        assertThrows(InvalidGiftCertCreationException.class, ()-> giftCertService.saveGiftCert(input, "COMPUTERS"));

        //missing 1 value
        input.setGiftCertName("name");
        assertThrows(InvalidGiftCertCreationException.class, ()-> giftCertService.saveGiftCert(input, null));
        assertThrows(InvalidGiftCertCreationException.class, ()-> giftCertService.saveGiftCert(input, "COMPUTERS"));

        input.setGiftCertName(null);
        input.setPrice(BigDecimal.valueOf(1));
        assertThrows(InvalidGiftCertCreationException.class, ()-> giftCertService.saveGiftCert(input, null));
        assertThrows(InvalidGiftCertCreationException.class, ()-> giftCertService.saveGiftCert(input, "COMPUTERS"));

        input.setDuration(null);
        input.setGiftCertName("name");
        assertThrows(InvalidGiftCertCreationException.class, ()-> giftCertService.saveGiftCert(input, null));
        assertThrows(InvalidGiftCertCreationException.class, ()-> giftCertService.saveGiftCert(input, "COMPUTERS"));

        //invalid duration
        input.setDuration(0);
        assertThrows(InvalidGiftCertCreationException.class, ()-> giftCertService.saveGiftCert(input, null));
        assertThrows(InvalidGiftCertCreationException.class, ()-> giftCertService.saveGiftCert(input, "COMPUTERS"));
    }

    @Test
    void modifyWithValidInputTest(){
        GiftCert expected = new GiftCert();
        expected.setId(5);
        expected.setGiftCertName("Summer Clothing");
        expected.setDescription("valid for clothing");
        expected.setPrice(BigDecimal.valueOf(700.7));
        expected.setDuration(40);
        expected.setCreateDate(LocalDateTime.now());
        expected.setLastUpdateDate(LocalDateTime.now());

        GiftCert input = new GiftCert();
        input.setGiftCertName("Summer Clothing");
        input.setDescription("valid for clothing");
        input.setPrice(BigDecimal.valueOf(700.7));
        input.setDuration(40);

        //no tags
        when(giftCertsRepository.updateGiftCert(5, input)).thenReturn(expected);
        when(giftCertsRepository.getTagsByGiftCertId(5)).thenReturn(null);
        GiftCert result = giftCertService.modifyGiftCert(5, input, null);
        assertEquals(result, expected, "atributes should be modified");

        //with tags
        ArrayList<Tag> tags = new ArrayList<>();
        Tag summerTag = new Tag(2, "SUMMER");
        Tag clothingTag = new Tag(3, "CLOTHING");
        tags.add(summerTag);
        tags.add(clothingTag);
        expected.setTags(tags);
        when(giftCertsRepository.getTagsByGiftCertId(5)).thenReturn(tags);
        when(giftCertsRepository.getTagByName("SUMMER")).thenReturn(summerTag);
        when(giftCertsRepository.getTagByName("CLOTHING")).thenReturn(clothingTag);
        result = giftCertService.modifyGiftCert(5, input, "SUMMER;CLOTHING");
        assertEquals(result, expected, "atributes should be modified");

        //ignore id, and dates
        input.setId(72);
        input.setCreateDate(LocalDateTime.MAX);
        input.setLastUpdateDate(LocalDateTime.MIN);
        result = giftCertService.modifyGiftCert(5, input, "SUMMER;CLOTHING");
        assertEquals(result, expected, "atributes should be modified");

    }

    @Test
    void modifyWithInvalidInputShouldThrowExceptionTest(){
        GiftCert input = new GiftCert();

        //null atributes
        assertThrows(InvalidModificationException.class, () -> giftCertService.modifyGiftCert(6, input, null));
        assertThrows(InvalidModificationException.class, () -> giftCertService.modifyGiftCert(6, input, "COMPUTERS"));

        //dates and id cannot be modified
        input.setId(72);
        input.setCreateDate(LocalDateTime.MAX);
        input.setLastUpdateDate(LocalDateTime.MIN);
        assertThrows(InvalidModificationException.class, () -> giftCertService.modifyGiftCert(6, input, null));
        assertThrows(InvalidModificationException.class, () -> giftCertService.modifyGiftCert(6, input, "COMPUTERS"));

        //giftcert with given id not found
        when(giftCertsRepository.updateGiftCert(0, input)).thenReturn(null);
        input.setGiftCertName("name");
        assertThrows(ResourceNotFoundException.class, () -> giftCertService.modifyGiftCert(0, input, null));
        assertThrows(ResourceNotFoundException.class, () -> giftCertService.modifyGiftCert(0, input, "COMPUTERS"));


    }

    @Test
    void dropExistingGiftCertTest(){
        when(giftCertsRepository.deleteGiftCert(7)).thenReturn(1);
        int result = giftCertService.dropGiftCert(7);
        assertEquals(result, 1);
    }

    @Test
    void dropNonExistingIdShouldThrowExceptionTest(){
        when(giftCertsRepository.deleteGiftCert(0)).thenReturn(0);
        assertThrows(ResourceNotFoundException.class, () -> giftCertService.dropGiftCert(0));
    }

    @Test
    void searchWithInvalidValuesShouldThrowExceptionTest(){
        //no values to search
        assertThrows(InvalidGiftCertSearchException.class, () ->
                giftCertService.searchGiftCerts(null, null, null, null, false));

        //tag doesn't exist
        when(giftCertsRepository.getTagByName("UNICORNIO")).thenReturn(null);
        assertThrows(NoMatchFoundException.class, () ->
                giftCertService.searchGiftCerts(null, null, "UNICORNIO", null, true));

        //no match
        when(giftCertsRepository.searchGiftCertByNameDescriptionTag("Automovile", "car",
                8, "date", true)).thenReturn(new ArrayList<>());
        when(giftCertsRepository.getTagByName("CARS")).thenReturn(new Tag(8, "CARS"));

        assertThrows(NoMatchFoundException.class, () ->
                giftCertService.searchGiftCerts("Automovile", "car", "CARS",
                        "date", true));
    }

}
