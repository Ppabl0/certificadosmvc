package com.epam.esm;

import com.epam.esm.model.GiftCert;
import com.epam.esm.repository.GiftCertRowMapper;
import com.epam.esm.repository.H2GiftCertsRepo;
import com.epam.esm.repository.TagRowMapper;
import org.junit.jupiter.api.*;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class RepositoryTest {
    private static EmbeddedDatabase dataSource;
    private static H2GiftCertsRepo repository;
    private static LocalDateTime localDateTime;

    @BeforeAll
    static void databaseSetUp(){
        dataSource = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addDefaultScripts()
                .build();
        localDateTime = LocalDateTime.now();

        repository = new H2GiftCertsRepo(dataSource,
                new GiftCertRowMapper(),
                new TagRowMapper());
    }

    @Test
    void addGiftCertTest(){
        GiftCert expected1 = new GiftCert(6, "Sillas", "certificado valido en sillas ergonomicas",
                BigDecimal.valueOf(1500), 60, localDateTime, localDateTime, null);
        assertEquals(1, repository.addGiftCert(expected1));
        GiftCert result = repository.getGiftCertById(6);
        assertTrue(fieldsAreEqual(expected1, result));

        GiftCert expected2 = new GiftCert(7, "Mesas", null,
                BigDecimal.valueOf(5500), 60, localDateTime, localDateTime, null);
        assertEquals(1, repository.addGiftCert(expected2));
        GiftCert result2 = repository.getGiftCertById(7);
        expected2.setDescription("");
        assertTrue(fieldsAreEqual(expected2, result2));

    }

    @Test
    void findGiftCertByIdTest(){
        GiftCert expected = new GiftCert(1, "Ropa caballeros", "canjeable en ropa para caballeros",
                BigDecimal.valueOf(500.00), 30, localDateTime, localDateTime, null);

        GiftCert result = repository.getGiftCertById(1);
        assertTrue(fieldsAreEqual(expected, result));
    }

    @Test
    void findGiftCertByNameTest(){
        GiftCert expected = new GiftCert(1, "Ropa caballeros", "canjeable en ropa para caballeros",
                BigDecimal.valueOf(500.00), 30, localDateTime, localDateTime, null);

        GiftCert result = repository.findGiftCertByName("Ropa caballeros");
        assertTrue(fieldsAreEqual(expected, result));
    }

    @Test
    void searchGiftCertByTagTest(){
        GiftCert item1 = new GiftCert(3, "Videojuegos", "canjeable en videojuegos, consolas, mandos del dpto videojuegos",
                BigDecimal.valueOf(1000.00).setScale(2), 60, localDateTime, localDateTime, null);
        GiftCert item2 = new GiftCert(4, "Devolucion de articulo", "devolucion en tarjeta de regalo canjeable en cualquier dpto",
                BigDecimal.valueOf(975.5), 365, localDateTime, localDateTime, null);

        List<GiftCert> result = repository.searchGiftCertByNameDescriptionTag(null, null, 4, "", false);
        assertTrue(fieldsAreEqual(item1, result.get(0)) && fieldsAreEqual(item2, result.get(1)));


    }

    @Test
    void updateGiftCertTest(){
        GiftCert expected = new GiftCert(5, "Nuevo name", "description sample",
                BigDecimal.valueOf(32), 60, localDateTime, localDateTime, null);

        GiftCert result = repository.updateGiftCert(5, expected);

        assertTrue(fieldsAreEqual(expected, result));
    }

    @Test
    void deleteGiftCertTest(){
        repository.deleteGiftCert(6);
        GiftCert result = repository.getGiftCertById(6);

        assertNull(result);
    }


    @AfterAll
    static void shutdownDatabase(){
        dataSource.shutdown();
    }


    private boolean fieldsAreEqual(GiftCert first, GiftCert second){
        return first.getId() == second.getId() &&
                first.getGiftCertName().equals(second.getGiftCertName()) &&
                first.getDescription().equals(second.getDescription()) &&
                first.getPrice().compareTo(second.getPrice()) == 0 &&
                first.getDuration().equals(second.getDuration()) &&
                compareLocalDateTime(first.getCreateDate(), second.getCreateDate()) == 0 &&
                compareLocalDateTime(first.getLastUpdateDate(), second.getLastUpdateDate()) == 0
                ;
    }

    private int compareLocalDateTime(LocalDateTime first, LocalDateTime second){
        int result;
        result = first.getYear() - second.getYear();
        result += first.getMonth().getValue() - second.getMonth().getValue();
        result += first.getDayOfWeek().getValue() - second.getDayOfWeek().getValue();
        result += first.getHour() - second.getHour();
        result += first.getMinute() - second.getMinute();
        result += first.getSecond() - second.getSecond();

        return result;
    }
}
