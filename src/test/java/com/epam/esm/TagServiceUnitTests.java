package com.epam.esm;

import com.epam.esm.exceptions.InvalidModificationException;
import com.epam.esm.exceptions.InvalidTagCreationException;
import com.epam.esm.exceptions.NoMatchFoundException;
import com.epam.esm.exceptions.ResourceNotFoundException;
import com.epam.esm.model.Tag;
import com.epam.esm.repository.GiftCertRepository;
import com.epam.esm.service.TagService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TagServiceUnitTests {
    @Mock
    private GiftCertRepository repository;
    @InjectMocks
    private TagService tagService;


    @Test
    void findTagByIdWithValidIdTest(){
        Tag expected = new Tag(1, "CYBERDAY");
        when(repository.getTagById(1)).thenReturn(expected);

        Tag result = tagService.findTagById(1);

        assertEquals(expected, result);
    }

    @Test
    void findTagByIdWithInvalidIdTest(){
        when(repository.getTagById(0)).thenReturn(null);

        assertThrows(ResourceNotFoundException.class, () -> tagService.findTagById(0), "should throw exception");
    }

    @Test
    void findTagByNameWithExistingTagNameTest(){
        Tag expected = new Tag(2, "CYBERDAY");
        when(repository.getTagByName("CYBERDAY")).thenReturn(expected);

        Tag result = tagService.findTagByName("CYBERDAY");

        assertEquals(expected, result);
    }

    @Test
    void findTagByNameWithNonExistingTagNameTest(){
        when(repository.getTagByName("FRUIT")).thenReturn(null);
        assertThrows(ResourceNotFoundException.class, () -> tagService.findTagByName("FRUIT"));
    }

    @Test
    void findAllTagsTest(){
        ArrayList<Tag> expected = new ArrayList<>();
        when(repository.getAllTags()).thenReturn(expected);
        //empty tags
        assertThrows(NoMatchFoundException.class, () -> tagService.findAllTags());

        //non-empty
        expected.add(new Tag(3, "MOTHERSDAY"));
        expected.add(new Tag(4, "HOT DRINKS"));
        List<Tag> result = tagService.findAllTags();
        assertEquals(expected, result);

    }

    @Test
    void addNewTagTest(){
        //null tag input
        assertThrows(InvalidTagCreationException.class, () -> tagService.addNewTag(null));

        //null tag name
        Tag input = new Tag(5, null);
        assertThrows(InvalidTagCreationException.class, () -> tagService.addNewTag(input));

        //valid tag name
        Tag expected = new Tag(72, "FURNITURE");
        when(repository.getTagByName("FURNITURE")).thenReturn(expected);
        input.setTagName("FURNITURE");

        Tag result = tagService.addNewTag(input);
        assertEquals(expected, result);

    }

    @Test
    void modifyTagTest(){
        //null input
        assertThrows(InvalidModificationException.class, () -> tagService.modifyTag(72, null));
        //null name
        Tag input = new Tag(99, null);
        assertThrows(InvalidModificationException.class, () -> tagService.modifyTag(72, input));
        //id doesn't exists
        input.setTagName("VEGETABLES");
        when(repository.updateTag(eq(72), any())).thenReturn(0);
        assertThrows(ResourceNotFoundException.class, () -> tagService.modifyTag(72, input));
        //id exists
        Tag expected = new Tag(73, "VEGETABLES");
        when(repository.updateTag(73, "VEGETABLES")).thenReturn(1);
        when(repository.getTagByName("VEGETABLES")).thenReturn(expected);

        Tag result = tagService.modifyTag(73, input);
        assertEquals(expected, result);
    }

    @Test
    void dropTagTest(){
        //id doesn't exists
        when(repository.deleteTag(99)).thenReturn(0);
        assertThrows(ResourceNotFoundException.class, () -> tagService.dropTag(99));
        //id exists
        when(repository.deleteTag(72)).thenReturn(1);
        assertTrue(tagService.dropTag(72) > 0);
    }


}
